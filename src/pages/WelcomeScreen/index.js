import UserDataForm from "../../components/UserDataForm";
import "./styles.css";

const WelcomeScreen = () => {
  return (
    <div className="welcome-container">
      <h1 className="welcome-message">
        Welcome to our chatbot system🎉 Please enter your name and age to enjoy
        the full experience
      </h1>
      <UserDataForm />
    </div>
  );
};

export default WelcomeScreen;
