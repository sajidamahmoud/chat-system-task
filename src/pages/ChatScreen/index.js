import ChatContent from "../../components/ChatContent";
import ChatHeader from "../../components/ChatHeader";
import ChatInputForm from "../../components/ChatInputForm";

const ChatScreen = () => {
  return (
    <>
      <ChatHeader />
      <ChatContent />
      <ChatInputForm />
    </>
  );
};

export default ChatScreen;
