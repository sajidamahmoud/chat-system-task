import notFound from "../../assets/not-found.png";
import "./styles.css";
const NotFoundScreen = () => {
  return (
    <div className="not-found-container">
      <img src={notFound} alt="Not found" />
      <h1 className="not-found-text">
        I'm sorry😔 I could not find the page you are looking for.
      </h1>
    </div>
  );
};

export default NotFoundScreen;
