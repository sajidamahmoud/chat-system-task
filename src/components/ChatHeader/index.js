import { useContext } from "react";
import GlobalContext from "../../context/GlobalContext";
import Avatar from "../Avatar";
import "./styles.css";

const ChatHeader = () => {
  const { user } = useContext(GlobalContext);

  return (
    <div className="header-container">
      <h1 className="header-title">
        Hi <span className="bold-title">{user?.name || "Guest"}!</span>
      </h1>
      <Avatar imageSrc={user?.image} imageSize="small" />
    </div>
  );
};

export default ChatHeader;
