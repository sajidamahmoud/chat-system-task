import "./styles.css";

const UserDataSubmitButton = () => {
  return (
    <button className="submit-button" type="submit">
      Start Chatting
    </button>
  );
};

export default UserDataSubmitButton;
