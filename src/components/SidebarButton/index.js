import { useContext } from "react";
import arrow from "../../assets/small-arrow.png";
import GlobalContext from "../../context/GlobalContext";
import "./styles.css";

const SidebarButton = () => {
  const { hideSidebar, setHideSidebar } = useContext(GlobalContext);

  const handleSidebarToggle = () => {
    setHideSidebar((prev) => !prev);
  };

  return (
    <button className="sidebar-button" onClick={handleSidebarToggle}>
      <img
        src={arrow}
        alt="Arrow"
        className={hideSidebar ? "rotated-arrow" : ""}
      />
    </button>
  );
};

export default SidebarButton;
