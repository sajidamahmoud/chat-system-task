import { useContext } from "react";
import GlobalContext from "../../context/GlobalContext";
import SidebarButton from "../SidebarButton";
import UserCard from "../UserCard";
import "./styles.css";

const ChatSidebar = () => {
  const { hideSidebar } = useContext(GlobalContext);

  return (
    <aside className={`sidebar ${hideSidebar && "hidden-sidebar"}`}>
      <SidebarButton />
      <UserCard />
    </aside>
  );
};

export default ChatSidebar;
