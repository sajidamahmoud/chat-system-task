import { useCallback, useContext, useEffect, useRef, useState } from "react";
import GlobalContext from "../../context/GlobalContext";
import generateMessageId from "../../utils/generateMessageId";
import Message from "../Message";
import MessageLoader from "../MessageLoader";
import { defaultReplyMessage, welcomeMessage } from "./botMessages";
import "./styles.css";

const MessagesBox = () => {
  const [messages, setMessages] = useState([welcomeMessage]);
  const [isBotMessageLoading, setIsBotMessageLoading] = useState(false);
  const { userMessage, hideSidebar } = useContext(GlobalContext);
  const bottomOfMessages = useRef();

  const scrollToBottom = useCallback(() => {
    bottomOfMessages.current.scrollIntoView({ behavior: "smooth" });
  }, []);

  // This is a dummy implementation of how the bot would reply to the user.
  const apppendBotMessage = useCallback(() => {
    setIsBotMessageLoading(true);
    const botReply = { ...defaultReplyMessage, id: generateMessageId() };

    setTimeout(() => {
      setMessages((prevMessages) => [botReply, ...prevMessages]);
      setIsBotMessageLoading(false);
      scrollToBottom();
    }, 500);
  }, [scrollToBottom]);

  useEffect(() => {
    if (Object.keys(userMessage).length !== 0 && userMessage.content !== "") {
      setMessages((prevMessages) => [userMessage, ...prevMessages]);
      apppendBotMessage();
    }
  }, [userMessage, userMessage.id, apppendBotMessage]);

  return (
    <section
      className={`messages-box ${hideSidebar && "expanded-messages-box"}`}
    >
      <div ref={bottomOfMessages}></div>
      {isBotMessageLoading && <MessageLoader />}
      {messages?.map((message) => (
        <Message
          key={message.id}
          content={message.content}
          sender={message.sender}
        />
      ))}
    </section>
  );
};

export default MessagesBox;
