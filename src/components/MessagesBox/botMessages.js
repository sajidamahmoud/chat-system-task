export const welcomeMessage = {
  sender: "bot",
  content:
    "Hello! this is your bot assistant. Is there something I can help you with?",
  id: 0,
};

export const defaultReplyMessage = {
  sender: "bot",
  content:
    "I'm sorry. I'm not a trained model. I cannot answer your questions yet :(",
  //No id is set here as this message is used more than once. A new ID is generated for each.
};
