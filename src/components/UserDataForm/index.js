import { useContext, useState } from "react";
import { useHistory } from "react-router";
import GlobalContext from "../../context/GlobalContext";
import validateUserAge from "../../utils/validateUserAge";
import validateUserName from "../../utils/validateUserName";
import UserDataInputField from "../UserDataInputField";
import UserDataSubmitButton from "../UserDataSubmitButton";
import "./styles.css";

const UserDataForm = () => {
  const [userName, setUserName] = useState("");
  const [userAge, setUserAge] = useState("");
  const [userNameError, setUserNameError] = useState("");
  const [userAgeError, setUserAgeError] = useState("");
  const { setUser } = useContext(GlobalContext);
  const history = useHistory();
  const avatarPlaceholder = "https://www.w3schools.com/w3images/avatar3.png";

  const handleUserNameChange = (event) => {
    setUserNameError("");
    setUserName(event.target.value);
  };

  const handleUserAgeChange = (event) => {
    setUserAgeError("");
    setUserAge(event.target.value);
  };

  const handleSetUserData = (event) => {
    event.preventDefault();
    let nameValidation = validateUserName(userName.trim());
    let ageValidation = validateUserAge(userAge);
    setUserNameError(nameValidation);
    setUserAgeError(ageValidation);
    if (!nameValidation && !ageValidation) {
      setUser({
        name: userName.trim(),
        age: userAge,
        image: avatarPlaceholder,
      });
      history.push("/chat");
    }
  };

  return (
    <form className="user-data-form" onSubmit={handleSetUserData}>
      <UserDataInputField
        value={userName}
        onChange={handleUserNameChange}
        errorMessage={userNameError}
        placeholder="Your Name"
        type="text"
      />
      <UserDataInputField
        value={userAge}
        onChange={handleUserAgeChange}
        errorMessage={userAgeError}
        placeholder="Your Age"
        type="number"
      />
      <UserDataSubmitButton />
    </form>
  );
};

export default UserDataForm;
