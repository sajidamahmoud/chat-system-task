import { useContext } from "react";
import GlobalContext from "../../context/GlobalContext";
import Avatar from "../Avatar";
import "./styles.css";

const UserCard = () => {
  const { user } = useContext(GlobalContext);

  return (
    <div className="user-card">
      <Avatar imageSrc={user?.image} imageSize="large" />
      <p className="user-card-text">Name: {user?.name || "Guest"}</p>
      <p className="user-card-text">Age: {user?.age || "20"} years</p>
    </div>
  );
};

export default UserCard;
