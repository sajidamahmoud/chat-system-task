import { useContext, useState } from "react";
import GlobalContext from "../../context/GlobalContext";
import generateMessageId from "../../utils/generateMessageId";
import SendButton from "../SendButton";
import "./styles.css";

const ChatInputForm = () => {
  const [message, setMessage] = useState("");
  const { setUserMessage } = useContext(GlobalContext);

  const handleSendMessage = (event) => {
    event.preventDefault();
    if (message !== "") {
      let messageObj = {
        content: message.trim(),
        sender: "user",
        id: generateMessageId(),
      };
      setUserMessage(messageObj);
      setMessage("");
    }
  };

  const handleMessageTyping = (event) => {
    setMessage(event.target.value);
  };

  return (
    <form className="message-form" onSubmit={handleSendMessage}>
      <input
        type="text"
        value={message}
        onChange={handleMessageTyping}
        className="message-input-field"
        placeholder="Type your message here"
      />
      <SendButton />
    </form>
  );
};

export default ChatInputForm;
