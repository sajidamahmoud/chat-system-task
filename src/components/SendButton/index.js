import plane from "../../assets/paper-plane.png";
import "./styles.css";

const SendButton = () => {
  return (
    <button className="send-button" type="submit">
      <img src={plane} alt="Send message" />
    </button>
  );
};

export default SendButton;
