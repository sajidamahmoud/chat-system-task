import loader from "../../assets/loading.svg";
import "./styles.css";

const MessageLoader = () => {
  return (
    <div className="loader-container">
      <img src={loader} alt="Loader" className="loader-image" />
    </div>
  );
};

export default MessageLoader;
