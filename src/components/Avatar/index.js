import "./styles.css";

const Avatar = ({ imageSrc, imageSize = "small" }) => {
  return (
    <div
      className={`avatar-container ${
        imageSize === "large" ? "large-avatar" : "small-avatar"
      }`}
    >
      <img src={imageSrc} alt="User avatar" className="avatar-image" />
    </div>
  );
};

export default Avatar;
