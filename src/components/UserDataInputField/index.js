import "./styles.css";

const UserDataInputField = ({
  value,
  type,
  placeholder,
  errorMessage,
  onChange,
}) => {
  return (
    <div className="user-input-container">
      <input
        value={value}
        type={type}
        onChange={onChange}
        placeholder={placeholder}
        className="user-input-field"
      />
      <p className="error-message">{errorMessage}</p>
    </div>
  );
};

export default UserDataInputField;
