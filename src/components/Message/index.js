import "./styles.css";

const Message = ({ content, sender }) => {
  return (
    <div className="message-container">
      <div
        className={`message ${
          sender === "bot" ? "bot-message" : "user-message"
        }`}
      >
        <p className="message-content">{content}</p>
      </div>
    </div>
  );
};

export default Message;
