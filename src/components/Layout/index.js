import "./styles.css";

const Layout = ({ children }) => {
  return (
    <div className="outer-container">
      <div className="content-container">{children}</div>
    </div>
  );
};

export default Layout;
