import ChatSidebar from "../ChatSidebar";
import MessagesBox from "../MessagesBox";
import "./styles.css";

const ChatContent = () => {
  return (
    <div className="chat-content">
      <MessagesBox />
      <ChatSidebar />
    </div>
  );
};

export default ChatContent;
