import { Route, Switch } from "react-router";
import { BrowserRouter } from "react-router-dom";
import "./App.css";
import Layout from "./components/Layout";
import ContextProvider from "./context/ContextProvider";
import ChatScreen from "./pages/ChatScreen";
import NotFoundScreen from "./pages/NotFoundScreen";
import WelcomeScreen from "./pages/WelcomeScreen";

const App = () => {
  return (
    <ContextProvider>
      <Layout>
        <BrowserRouter>
          <Switch>
            <Route exact path="/chat" component={ChatScreen} />
            <Route exact path="/" component={WelcomeScreen} />
            <Route component={NotFoundScreen} />
          </Switch>
        </BrowserRouter>
      </Layout>
    </ContextProvider>
  );
};

export default App;
