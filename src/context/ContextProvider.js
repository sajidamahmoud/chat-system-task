import { useState } from "react";
import GlobalContext from "./GlobalContext";

const ContextProvider = ({ children }) => {
  const avatarPlaceholder = "https://www.w3schools.com/w3images/avatar3.png";
  const defaultUser = {
    name: "Guest",
    age: "20",
    image: avatarPlaceholder,
  };
  const [user, setUser] = useState(defaultUser);
  const [userMessage, setUserMessage] = useState({});
  const [hideSidebar, setHideSidebar] = useState(false);

  const context = {
    userMessage,
    setUserMessage,
    hideSidebar,
    setHideSidebar,
    user,
    setUser,
  };

  return (
    <GlobalContext.Provider value={context}>{children}</GlobalContext.Provider>
  );
};

export default ContextProvider;
