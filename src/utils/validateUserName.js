const validateUserName = (userName) => {
  const lettersOnlyRegEx = /^[a-zA-Z\s]*$/;

  if (userName === "") {
    return "Please enter your name";
  } else if (!lettersOnlyRegEx.test(userName)) {
    return "Your name should only contain letters";
  }
};

export default validateUserName;
