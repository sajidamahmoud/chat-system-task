const validateUserAge = (userAge) => {
  const numbersOnlyRegEx = /^\d+$/;

  if (userAge === "") {
    return "Please enter your age";
  } else if (!numbersOnlyRegEx.test(userAge)) {
    return "Your age should only contain numbers";
  } else if (parseInt(userAge) < 1 || parseInt(userAge) > 150) {
    return "Please enter a realistic age";
  }
};

export default validateUserAge;
