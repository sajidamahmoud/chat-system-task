const generateMessageId = () => {
  return parseInt(Date.now() + Math.random());
};

export default generateMessageId;
